#include <mpi.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <cstring>
#include <vector>

#define ROOT 0

void genP(int i, std::string& s, std::vector<int>& pi) {
	for (int k = i; k >= 0; --k)
		if (s.substr(0, k) == s.substr(i - k + 1, k)) {
			pi[i] = k;
			break;
		}
}

void prefix_function(std::string s, int rank, int size, std::string pattern) {
	int n = (int)s.length();
	std::vector<int> pi(n);
	int i = rank;
	int j = n - rank - 1;
	while (!(i >= n / 2 || j <= n / 2 - 1)) {
		genP(i, s, pi);
		genP(j, s, pi);

		if (pi[i] == pattern.length())
			printf("[%d]: pattern '%s' found at %d position.\n", rank, pattern.c_str(), i - 2 * pattern.length());
		if (pi[j] == pattern.length())
			printf("[%d]: pattern '%s' found at %d position.\n", rank, pattern.c_str(), j - 2 * pattern.length());
		i += size;
		j -= size;
	}
	//printf("loop[%i] end when i = %i and j = %i\n", rank, i, j);
	//for (int i = startIndex; i < endIndex; ++i)
	//	genP(i, s, pi);
		/*for (int k = i; k >=0; --k)
			if (s.substr(0, k) == s.substr(i - k + 1, k)) {
				pi[i] = k;
				break;
			}*/
	//return pi;
}

int main(int argc, char* argv[]) {
    int rank, size, inputLength, patternLength;
    double start = 0.0;
    double finish = 0.0;
	char* inputString;
	char* patternString;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    MPI_Comm_size (MPI_COMM_WORLD, &size);

    if (rank == ROOT) {

		std::string inputFilename = "D:\match.txt";
		std::ifstream is (inputFilename);
        if (is) {
            is.seekg(0, is.end);
			inputLength = is.tellg();
            is.seekg(0, is.beg);

			inputString = new char[inputLength];

            is.read(inputString, inputLength);

            if (is)
				printf("'%s' open is Ok\n", inputFilename.c_str());
            else {
                printf("only %li could be read\n", is.gcount());
                return -1;
            }
            is.close();
        } else {
            printf("Couldn't read file\n");
            return  -1;
        }

		std::string patterFilename = "D:\input.txt";
		std::ifstream isPattern(patterFilename);
		if (isPattern) {
			isPattern.seekg(0, isPattern.end);
			patternLength = isPattern.tellg();
			isPattern.seekg(0, isPattern.beg);

			patternString = new char[patternLength];

			isPattern.read(patternString, patternLength);

			if (isPattern)
				printf("'%s' open is Ok\n", patterFilename.c_str());
			else {
				printf("only %li could be read\n", isPattern.gcount());
				return -1;
			}
			is.close();
		}
		else {
			printf("Couldn't read file\n");
			return  -1;
		}


        printf("Starting programm.\n");
        start = MPI_Wtime();
    }

    MPI_Bcast(&inputLength, 1, MPI_LONG, ROOT, MPI_COMM_WORLD);
	MPI_Bcast(&patternLength, 1, MPI_LONG, ROOT, MPI_COMM_WORLD);

    if (rank != ROOT) {
		inputString = new char[inputLength];
		patternString = new char[patternLength];
    }

	MPI_Bcast(inputString, inputLength, MPI_CHAR, ROOT, MPI_COMM_WORLD);
	MPI_Bcast(patternString, patternLength, MPI_CHAR, ROOT, MPI_COMM_WORLD);
	

	std::string pattern(patternString, patternLength);
	std::string match(inputString, inputLength);

	std::string all = pattern + "#" + match;
	prefix_function(all, rank, size, pattern);
	/*for (int i = 0; i < v.size(); ++i) {
		if (v[i] == patternLength)
			printf("[%d]: pattern '%s' found at %d position.\n", rank, pattern.c_str(), i - 2 * patternLength);
	}*/

    if (rank == ROOT) {
        finish = MPI_Wtime();
        printf("[%d]: time - %f.\n", rank, finish - start);
    }

    MPI_Finalize();

	//_getch();
    return 0;
}